import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;
import res.GUI;
import res.Settings;
import tasks.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.net.URL;
import java.util.concurrent.TimeUnit;

@ScriptMeta(developer = "Constellation", desc = "Picks Cavada and Redberries near Varrock", name = "Berry Forager", version = 1.0)
public class BerryForager extends TaskScript implements RenderListener {

    //An array containing all of the Tasks to be used by the script
    private static final Task[] TASKS = {new Pick(), new Hop(), new UseBank(), new Traverse()};

    //Paint variables
    private Font helvitica = new Font("Helvetica", Font.PLAIN, 13);
    private Font centuryGothic = new Font("CenturyGothic", Font.BOLD, 14);
    private Image bg = getImage("https://i.imgur.com/N5EI2GD.png");
    private long startTime;
    private final int PRICE_REDBERRY = 36;
    private final int PRICE_CADAVA = 23;

    @Override
    public void onStart()
    {

        new GUI().setVisible(true);

        Log.info("Starting SCRIPT: Berry Forager");

        //Get the current system time in milliseconds so we know the exact time the script started
        //Used for runtime calculation for the Paint
        startTime = System.currentTimeMillis();

        //Get the current inventory count for calculating the amount of berries picked
        Settings.inventoryCount = Inventory.getCount();

        //Submit the Tasks defined above to the TaskScript loop.
        //The way a TaskScript RSPeer script works is like this:
        //In a loop, one-by-one the validate() method for each Task is called, if the validate() method
        //returns true, then the execute() method of the task is run. Then the next task is checked and so on,
        //this continues to loop until the script is stopped. The loop and calling the validate() methods are handled
        //internally by TaskScript, you don't have to worry about that, just submit the tasks.
        submit(TASKS);
    }

    @Override
    public void onStop() {
        Log.info("Stopping SCRIPT: Berry Forager");
        Log.info("Thank you for using Constellation's Berry Forager!");
    }

    //Paint method
    @Override
    public void notify(RenderEvent renderEvent) {
        //Get graphics
        Graphics g = renderEvent.getSource();

        //Update Variables
        //Get the running time of the script in ms and store it in a long variable
        long runningTime = System.currentTimeMillis() - startTime;
        //Calculate berries per hour by using the static variable: picked. from Settings.java
        int berriesPerHour = (int) (Settings.picked / ((System.currentTimeMillis() - startTime) / 3600000.0D));
        //Calculates the gp gained
        int gpGained = Settings.cadava * PRICE_CADAVA + Settings.redberry * PRICE_REDBERRY;
        //Calculates gp per hour
        int gpPerHour = (int) (gpGained / ((System.currentTimeMillis() - startTime) / 3600000.0D));

        Color transparentBlack = new Color(0,0,0, 100);
        Color translucentWhite = new Color(255, 255, 255, 150);

        //Gussy up the text
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

        //Draw the Background
        g2d.setColor(transparentBlack);
        g2d.fillRoundRect(5, 275, 365, 60, 15, 15);
        //g2d.fillRect(315, 215, 200, 120);
        g2d.setColor(translucentWhite);
        g2d.drawRoundRect(5, 275, 366, 61, 15, 15);

        //Draw Title
        g2d.setColor(transparentBlack);
        g2d.fillRoundRect(10, 240, 130, 23, 15, 15);
        g2d.setColor(translucentWhite);
        g2d.drawRoundRect(10, 240, 131, 24, 15, 15);
        //Draw avatar image
        g.drawImage(bg, 124, 236, null);
        //Draw Paint Text
        g2d.setColor(Color.WHITE);
        g2d.setFont(centuryGothic);
        g2d.drawString("Berry Forager", 20, 257);

        //Draw Paint info
        g2d.setFont(helvitica);
        g2d.setColor(Color.WHITE);
        g2d.drawString("Runtime: " + formatTime(runningTime), 20, 300);
        g2d.drawString("Picked: " + Integer.toString(Settings.picked) + " (" + Integer.toString(berriesPerHour) + ")", 20, 323);
        g2d.drawString("Script: " + Settings.state, 165, 300);
        g2d.drawString("GP Gained: " + gpGained + " (" + gpPerHour + ")", 165, 323);
    }

    //Simple method to get an image from a URL, in this case an imgur link
    private Image getImage(String url){
        try{
            return ImageIO.read(new URL(url));
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    //Format time given in long into a readable format: HH:MM:SS
    //Used by the Paint
    private String formatTime(long r){

        //long days = TimeUnit.MILLISECONDS.toDays(r);
        long hours = TimeUnit.MILLISECONDS.toHours(r);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(r) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(r));
        long seconds = TimeUnit.MILLISECONDS.toSeconds(r) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(r));
        String res = "";

        //Pretty Print the time so it will always be in this format 00:00:00
        if( hours < 10 ){
            res = res + "0" + hours + ":";
        }
        else{
            res = res + hours + ":";
        }
        if(minutes < 10){
            res = res + "0" + minutes + ":";
        }
        else{
            res = res + minutes + ":";
        }
        if(seconds < 10){
            res = res + "0" + seconds;
        }
        else{
            res = res + seconds;
        }

        return res;
    }
}
