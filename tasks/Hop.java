package tasks;

import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.WorldHopper;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import res.Location;
import res.Settings;

public class Hop extends Task{

    // Object instance of the local player
    private Player local = Players.getLocal();

    @Override
    public boolean validate() {
        // Update the local player instance before validating, otherwise unexpected behaviour
        local = Players.getLocal();

        // If the players inventory is not full and if the player is in the BUSHES area
        // defined in Location.java and if there are no valid bushes to pick from then return true
        // and execute this task
        return Settings.start
                && !Inventory.isFull()
                && Location.BUSHES.getArea().contains(local)
                && SceneObjects.getNearest(Settings.BUSH) == null;
    }

    @Override
    public int execute() {
        //Get the nearest valid bush to double check that in case a bush has respawned
        SceneObject bush = SceneObjects.getNearest(Settings.BUSH);
        //If no bush has respawned and they are all empty, RSPeer will return a null object
        //If there is no nearby valid bush then...
        if(bush == null){
            //If the world hopper interface is not open then...
            if(!WorldHopper.isOpen()){
                //Open the world hopper interface
                WorldHopper.open();
                //Sleep until the interface is open, check every 50ms for 1.5 seconds
                Time.sleepUntil(() -> WorldHopper.isOpen(), 50, 1500);
            }
            //If the world hopper interface is open then...
            if(WorldHopper.isOpen()){
                //Update the paint variable
                Settings.state = "Hopping to Next World ";
                //Hop to the next world. Make sure the world is free,
                //and not a world where the character can get killed.
                if(WorldHopper.hopNext(rw -> !rw.isMembers()
                        && !rw.isHighRisk()
                        && !rw.isSkillTotal()
                        && !rw.isBounty()
                        && !rw.isPVP()
                        && !rw.isDeadman())){

                    //If this is a fresh character, this will be the first time the world hopper is used
                    //Handle the dialog that asks if you really want to change worlds.
                    //If the dialog chat options are on screen then...
                    if(Dialog.isViewingChat()){
                        //Click on the "Yes, only warn me in the future about dangerous worlds" option
                        Dialog.process(e -> e.contains("future"));
                    }

                    //Sleep until there are valid berry bushes to be picked from
                    Time.sleepUntil(() -> SceneObjects.getNearest(Settings.BUSH) != null, 100, 6000);
                    //Sleep for a random number of ms between 250ms to 750ms
                    Time.sleep(250, 750);
                }
            }
        }
        //The script will sleep for 250ms after task execution
        return 250;
    }

}
