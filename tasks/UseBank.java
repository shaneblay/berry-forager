package tasks;

import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import res.Location;
import res.Settings;

public class UseBank extends Task {

    // Object instance of the local player
    private Player local = Players.getLocal();

    @Override
    public boolean validate() {
        // Update the local player instance before validating, otherwise unexpected behaviour
        local = Players.getLocal();

        // If the local player is in the bank area defined in Location.java and
        // the player's inventory is full then the task will be executed.
        return Settings.start
                && Location.BANK.getArea().contains(local)
                && Inventory.isFull();
    }

    @Override
    public int execute() {
        // Update the state variable for the script Paint located in BerryForager.java
        Settings.state = "Using Bank";

        // If the bank is not open then...
        if(!Bank.isOpen()){
            // Open the bank, if the bank was successfully interacted with by RSPeer then...
            if(Bank.open()){
                // Sleep until the bank is open, check the condition every 50ms,
                // if after 2 seconds the bank is still not open, stop sleeping.
                Time.sleepUntil(() -> Bank.isOpen(), 50, 2000);
                // Sleep for a random number of ms between 250ms - 750ms
                Time.sleep(250,750);
            }
        }
        // If the bank is open then...
        if(Bank.isOpen()){
            // Deposit entire inventory using the button. If successfully pressed then...
            if(Bank.depositInventory()) {
                // Sleep until the inventory is empty, check the condition every 50ms for 1 second
                Time.sleepUntil(() -> Inventory.isEmpty(), 50, 1000);
                //Resets the tracked inventory count so the counter in the Paint doesn't mistakenly go up
                Settings.inventoryCount = 0;
            }
            // Sleep for a random number of ms between 250ms - 750ms
            Time.sleep(250,750);
            // Close the bank interface
            Bank.close();
        }
        // Script sleeps for 300ms after task execution
        return 300;
    }
}
