package res;

import org.rspeer.runetek.adapter.scene.SceneObject;

import java.util.function.Predicate;


public class Settings {
    public static int picked = 0;
    public static int cadava = 0;
    public static int redberry = 0;
    /** The state variable used to pass info to the Paint **/
    public static String state = "Loading...";
    /** The predicate used to pick berries following user preferences**/
    public static Predicate<SceneObject> BUSH = b -> b.getId() == 23625
            || b.getId() == 23626
            || b.getId() == 23628
            || b.getId() == 23629;
    /** Boolean used to activate script after Start Script button in GUI is pressed **/
    public static int energyActivation = org.rspeer.runetek.api.commons.math.Random.nextInt(70,100);
    public static int inventoryCount = 0;
    public static String lastBerry = "";
    public static boolean start = false;
}
