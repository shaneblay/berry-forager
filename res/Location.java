package res;


import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

public enum Location {
    /** Surrounding area of all cadava and redberry bushes **/
    BUSHES(Area.rectangular(new Position(3260,3376,0), new Position(3280,3361,0))),

    /** Used to determine if the player near the bank**/
    BANK(Area.rectangular(new Position(3247,3430,0), new Position(3262,3412,0))),

    /** Used to walk to the closest bush from the bank **/
    BERRY_AREA(Area.rectangular(new Position(3274,3374,0), new Position(3278,3371,0)));

    private Area area;

    Location(Area area){
        this.area = area;
    }

    public Area getArea(){
        return area;
    }
}
